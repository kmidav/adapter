import Vue from 'vue';
import ShopApp from "./components/ShopApp";
import 'bootstrap/dist/css/bootstrap.css';

Vue.component('shop-app', ShopApp);

const app = new Vue({
    el: '#shop-app'
});
