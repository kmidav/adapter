<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210314061715 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product_offer ADD owner_id INT NOT NULL');
        $this->addSql('ALTER TABLE product_offer ADD CONSTRAINT FK_888AFC627E3C61F9 FOREIGN KEY (owner_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_888AFC627E3C61F9 ON product_offer (owner_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product_offer DROP FOREIGN KEY FK_888AFC627E3C61F9');
        $this->addSql('DROP INDEX IDX_888AFC627E3C61F9 ON product_offer');
        $this->addSql('ALTER TABLE product_offer DROP owner_id');
    }
}
