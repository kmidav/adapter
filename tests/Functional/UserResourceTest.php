<?php


namespace App\Tests\Functional;


use App\Entity\User;
use App\Test\BaseApiTestCase;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;

class UserResourceTest extends BaseApiTestCase
{
    use ReloadDatabaseTrait;

    public function testCreateUser(): void
    {
        $client = self::createClient();
        $client->request('POST', '/api/users', [
            'json' => [
                'email' => 'user1@mail.ru',
                'username' => 'user1',
                'password' => '123'
            ]
        ]);
        self::assertResponseStatusCodeSame(201);

        $this->login($client, 'user1@mail.ru', '123');
        self::assertResponseStatusCodeSame(204);
    }

    public function testUpdateUser(): void
    {
        $client = self::createClient();
        $user = $this->createUserAndLogin($client, 'user1@mail.ru', 'user1', '123');
        $client->request('PUT', '/api/users/'.$user->getId(), [
            'json' => [
                'username' => 'user1new',
                'roles'    => ['ROLE_ADMIN'],
            ]
        ]);
         self::assertResponseIsSuccessful();

        self::assertJsonContains([
            'username' => 'user1new'
        ]);


        $em = $this->getEntityManager();
        $user = $em->getRepository(User::class)->find($user->getId());
        self::assertEquals(['ROLE_USER'], $user->getRoles());
    }

    public function testGetUser(): void
    {
        $client = self::createClient();

        $user = $this->createUser('user1@mail.ru', 'user1', '123');
        $this->createUserAndLogin($client, 'user2@mail.ru', 'user2', '123');
        $user->setPhoneNumber('89132458858');
        $em = $this->getEntityManager();
        $em->persist($user);
        $em->flush();
        // пользователи не могут видеть чужие телефонные номера, только свои
        // только ADMIN может и владелец своей сущности  todo убрать это поведение
        $client->request('GET', '/api/users/'.$user->getId());
        self::assertJsonContains([
            'username' => 'user1'
        ]);
        // и проверим что json не пришел с полем телефон (phoneNumber)
        // todo сделать private метод для проверки не содержит ли json поля конкретного
        $userData = $client->getResponse()->toArray();
        // проверяет что в массиве нету ключа со значением таким
        $this->assertArrayNotHasKey('phoneNumber', $userData);

        $user = $em->getRepository(User::class)->find($user->getId());

        $user->setRoles(['ROLE_ADMIN', 'ROLE_USER']);
        $em->persist($user);
        $em->flush();
        $this->login($client, 'user1@mail.ru', '123');

        $client->request('GET', '/api/users/'.$user->getId());
        self::assertJsonContains([
            'phoneNumber' => '89132458858'
        ]);
    }
}