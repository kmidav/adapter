<?php


namespace App\DataProvider;


use ApiPlatform\Core\DataProvider\CollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\Pagination;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use ApiPlatform\Core\Exception\ResourceClassNotSupportedException;
use App\ApiPlatform\DailyReportDateFilter;
use App\Entity\DailyReport;
use App\Repository\ProductOfferRepository;
use App\Service\ReportHelper;

class DailyReportProvider implements ContextAwareCollectionDataProviderInterface, RestrictedDataProviderInterface, ItemDataProviderInterface
{
    /**
     * @var ReportHelper
     */
    private $reportHelper;
    /**
     * @var ProductOfferRepository
     */
    private $productOfferRepository;
    private Pagination $pagination;

    public function __construct(ReportHelper $reportHelper, Pagination $pagination, ProductOfferRepository $productOfferRepository)
    {
        $this->reportHelper = $reportHelper;
        $this->productOfferRepository = $productOfferRepository;
        $this->pagination = $pagination;
    }

    public function getCollection(string $resourceClass, string $operationName = null, array $context = [])
    {
        list($page, $offset, $limit) = $this->pagination
            ->getPagination($resourceClass, $operationName);

        $paginator = new DailyReportPaginator($this->reportHelper, $page, $limit);

        $fromDate = $context[DailyReportDateFilter::FROM_FILTER_CONTEXT] ?? null;
        if ($fromDate) {
            $paginator->setFromDate($fromDate);
        }

        return $paginator;
    }

    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = [])
    {
        return $this->reportHelper->fetchOne($id);
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return $resourceClass === DailyReport::class;
    }
}