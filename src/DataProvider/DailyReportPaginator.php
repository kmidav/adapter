<?php


namespace App\DataProvider;


use ApiPlatform\Core\DataProvider\PaginatorInterface;
use App\Service\ReportHelper;
use Exception;
use Traversable;

class DailyReportPaginator implements PaginatorInterface, \IteratorAggregate
{

    private $dailyReportsIterator;
    private ReportHelper $reportHelper;
    private int $currentPage;
    private int $maxResults;
    private $fromDate;

    public function __construct(ReportHelper $reportHelper, int $currentPage, int $maxResults)
    {
        $this->reportHelper = $reportHelper;
        $this->currentPage = $currentPage;
        $this->maxResults = $maxResults;
    }

    public function count()
    {
       return iterator_count($this->getIterator());
    }

    public function getLastPage(): float
    {
        return ceil($this->getTotalItems() / $this->getItemsPerPage()) ?: 1;
    }

    public function getTotalItems(): float
    {
        return $this->reportHelper->count();
    }

    public function getCurrentPage(): float
    {
        return $this->currentPage;
    }

    public function getItemsPerPage(): float
    {
        return $this->maxResults;
    }

    public function getIterator()
    {
        if ($this->dailyReportsIterator === null) {
            $offset = ($this->getCurrentPage() - 1) * $this->getItemsPerPage();

            $criteria = [];
            if ($this->fromDate) {
                $criteria['from'] = $this->fromDate;
            }
            $this->dailyReportsIterator = new \ArrayIterator(
                $this->reportHelper->fetchMany(
                    $this->getItemsPerPage(),
                    $offset,
                    $criteria
                )
            );
        }

        return $this->dailyReportsIterator;
    }

    /**
     * @param mixed $fromDate
     */
    public function setFromDate($fromDate): void
    {
        $this->fromDate = $fromDate;
    }
}