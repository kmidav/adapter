<?php


namespace App\DataProvider;


use ApiPlatform\Core\DataProvider\CollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\DenormalizedIdentifiersAwareItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Security;

class UserDataProvider implements ContextAwareCollectionDataProviderInterface, RestrictedDataProviderInterface, DenormalizedIdentifiersAwareItemDataProviderInterface
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var CollectionDataProviderInterface
     */
    private $collectionDataProvider;

    /**
     * @var Security
     */
    private $security;
    /**
     * @var ItemDataProviderInterface
     */
    private $itemDataProvider;

    public function __construct(UserRepository $userRepository, CollectionDataProviderInterface $collectionDataProvider, ItemDataProviderInterface $itemDataProvider, Security $security)
    {
        $this->collectionDataProvider = $collectionDataProvider;
        $this->security = $security;
        $this->itemDataProvider = $itemDataProvider;
    }

    public function getCollection(string $resourceClass, string $operationName = null, array $context = [])
    {
       /** @var User[] $users */
       $users = $this->collectionDataProvider->getCollection($resourceClass, $operationName);

       foreach ($users as $user) {
           $user->setIsMe(false);

           if ($this->security->getUser() === $user) {
               $user->setIsMe(true);
           }
       }

       return $users;
    }

    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = [])
    {
        /** @var User|null $user */
        $user = $this->itemDataProvider->getItem($resourceClass, $id, $operationName);

        if (!$user) {
            return null;
        }

        $user->setIsMe($this->security->getUser() === $user);

        return $user;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return $resourceClass === User::class;
    }
}