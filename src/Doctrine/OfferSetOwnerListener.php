<?php


namespace App\Doctrine;

use App\Entity\ProductOffer;
use App\Entity\User;
use Symfony\Component\Security\Core\Security;

class OfferSetOwnerListener
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function prePersist(ProductOffer $offer): void
    {
        if ($offer->getOwner()) {
            return;
        }
        /** @var User $user */
        $user = $this->security->getUser();

        if ($user) {
            $offer->setOwner($user);
        }
    }
}