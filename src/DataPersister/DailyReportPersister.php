<?php


namespace App\DataPersister;


use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use App\Entity\DailyReport;
use Psr\Log\LoggerInterface;

class DailyReportPersister implements DataPersisterInterface
{

    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function supports($data): bool
    {
        return $data instanceof DailyReport;
    }

    /**
     * @param DailyReport $data
     * @return object|void
     */
    public function persist($data)
    {
        // todo изменять данные в json файле или совсем отказаться от этой идеи собирать данные по другому
        // todo не в json файл
        $this->logger->info(sprintf('update visitors to %d', $data->totalVisitors));
    }

    public function remove($data)
    {
        return new \Exception('not supported');
    }
}