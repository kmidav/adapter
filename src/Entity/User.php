<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;

/**
 * @ApiResource(
 *     security="is_granted('ROLE_USER')",
 *     normalizationContext={"groups"={"user:read"}, "swagger_definition_name"="Read"},
 *     denormalizationContext={"groups"={"user:write"}, "swagger_definition_name"="Write"},
 *     itemOperations={
 *          "get",
 *          "put"={"security"="is_granted('ROLE_USER') and object == user"},
 *          "delete"={"security"="is_granted('ROLE_ADMIN')"}
 *     },
 *     collectionOperations={
 *          "get",
 *          "post"={
 *              "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *              "validation_groups" = {"Default", "create"}
 *          }
 *     },
 *     attributes={
 *          "pagination_items_per_page"=10,
 *     }
 * )
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(fields={"username"})
 * @UniqueEntity(fields={"email"})
 * @ORM\EntityListeners({"App\Doctrine\UserSetIsMvpListener"})
 * @ApiFilter(PropertyFilter::class)
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="uuid", unique=true)
     * @Groups({"user:write"})
     */
    private $uuid;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({"user:read", "user:write"})
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

     /**
     * @ORM\Column(type="json")
     * @Groups({"admin:write"})
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @Groups({"user:write"})
     * @SerializedName("password")
     * @Assert\NotBlank(groups={"create"})
     */
    private $plainPassword;

    /**
     * Return true if this currently authenticated user
     *
     * @Groups({"user:read"})
     */
    private $isMe = false;

    /**
     * Return true if user is MVP
     *
     * @Groups({"user:read"})
     */
    private $isMvp;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Groups({"user:read", "user:write", "offer:item:get"})
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min=4,
     *     max=20
     * )
     */
    private $username;

    /**
     * @ORM\OneToMany(targetEntity=ProductOffer::class, mappedBy="owner", cascade="persist", orphanRemoval=true)
     * @Groups({"user:write"})
     * @Assert\Valid()
//     * @ApiSubresource()
     */
    private $productOffers;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"admin:read", "user:write", "owner:read"})
     */
    private $phoneNumber;

    public function __construct(UuidInterface $uuid = null)
    {
        $this->productOffers = new ArrayCollection();
        $this->uuid = $uuid ?? Uuid::uuid4();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        $this->plainPassword = null;
    }

    /**
     * @return Collection|ProductOffer[]
     */
    public function getProductOffers(): Collection
    {
        return $this->productOffers;
    }

    // @ApiProperty(readableLink=true) то будет embedded
    /**
     //* @ApiProperty(readableLink=true)
     * @return Collection<ProductOffer>
     * @Groups({"user:read"})
     * @SerializedName("productOffers")
     *
     */
    public function getPublishedProductOffers(): Collection
    {
        return $this->productOffers->filter(function(ProductOffer $offer) {
            return $offer->getIsPublished();
        });
    }

    public function addProductOffer(ProductOffer $productOffer): self
    {
        if (!$this->productOffers->contains($productOffer)) {
            $this->productOffers[] = $productOffer;
            $productOffer->setOwner($this);
        }

        return $this;
    }

    public function removeProductOffer(ProductOffer $productOffer): self
    {
        if ($this->productOffers->removeElement($productOffer)) {
            // set the owning side to null (unless already changed)
            if ($productOffer->getOwner() === $this) {
                $productOffer->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    /**
     * @param mixed $plainPassword
     */
    public function setPlainPassword(string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;
        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsMe(): bool
    {
        return $this->isMe;
    }

    /**
     * @param bool $isMe
     */
    public function setIsMe(bool $isMe): void
    {
        $this->isMe = $isMe;
    }

    /**
     * @return mixed
     */
    public function getIsMvp()
    {
        return $this->isMvp;
    }

    /**
     * @param mixed $isMvp
     */
    public function setIsMvp($isMvp): void
    {
        $this->isMvp = $isMvp;
    }

    /**
     * @return UuidInterface
     */
    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }
}
