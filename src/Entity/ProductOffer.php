<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ProductOfferRepository;
use App\Validator\OfferOwnerValid;
use App\Validator\PublishedProductOfferValid;
use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use Symfony\Component\Validator\Constraints as Assert;
use App\ApiPlatform\ProductOfferFilter;
use App\Dto\ProductOfferOutput;
use App\Dto\ProductOfferInput;

/**
 * @ApiResource(
 *     output=ProductOfferOutput::class,
 *     input=ProductOfferInput::class,
 *     routePrefix="/product",
 *     collectionOperations={
 *      "get",
 *      "post"={
 *          "security"="is_granted('ROLE_USER')",
 *          "denormalization_context"={"groups"={"offer:write", "offer:collection:post"}}
 *      }
 *      },
 *     itemOperations={
 *      "get" = {
 *          "normalization_context"={"groups"={"offer:read", "offer:item:get"}},
 *          "path"="/offers/{id}.{_format}",
 *          "method"="GET",
 *          "requirements"={"id"="\d+"}
 *      },
 *      "put"={
 *        "security"="is_granted('EDIT', object)",
 *         "security_message"="only owner can update offer"
 *       },
 *      "delete"={"security"="is_granted('ROLE_ADMIN')"}
 *      },
 *     shortName="offer",
 *     normalizationContext={"groups"={"offer:read"}, "swagger_definition_name"="Read"},
 *     denormalizationContext={"groups"={"offer:write"}, "swagger_definition_name"="Write"},
 *     attributes={
 *          "pagination_items_per_page"=10,
 *          "formats"={"jsonld", "json", "html", "jsonhal", "csv"={"text/csv"}}
 *     }
 * )
 * @ApiFilter(BooleanFilter::class, properties={"isPublished"})
 * @ApiFilter(SearchFilter::class, properties={
 *     "name": "partial",
 *     "description": "partial",
 *     "owner": "exact",
 *     "owner.username": "partial"
 * })
 * @ApiFilter(ProductOfferFilter::class, arguments={"useLike"=true})
 * @ApiFilter(RangeFilter::class, properties={"price"})
 * @ApiFilter(PropertyFilter::class)
 * @ORM\Entity(repositoryClass=ProductOfferRepository::class)
 * @ORM\EntityListeners({"App\Doctrine\OfferSetOwnerListener"})
 * @PublishedProductOfferValid()
 */
class ProductOffer
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"offer:read", "offer:write", "user:read", "user:write"})
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min=2,
     *     max=50,
     *     maxMessage="Name must be 50 chars or less"
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"offer:read", "user:read", "user:write"})
     */
    private $description;

    /**
     * @ORM\Column(type="string", nullable=true, length=510)
     */
    private $shortDescription;

    /**
     * Price in rubles
     *
     * @ORM\Column(type="integer")
     * @Groups({"offer:read", "offer:write", "user:read", "user:write"})
     * @Assert\NotBlank()
     * @Assert\GreaterThan(value="0")
     */
    private $price;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $discountPrice;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"offer:write"})
     */
    private $isPublished = false;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="productOffers")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"offer:read", "offer:collection:post"})
     * @Assert\Valid()
     * @OfferOwnerValid()
     */
    private $owner;

    public function __construct(string $name)
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->name = $name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    // todo раскоментируй он нужен, сделай обязательным например vendor поставщика в construct
//    public function setName(string $name): self
//    {
//        $this->name = $name;
//
//        return $this;
//    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return string
     * @Groups({"offer:read"})
     * @SerializedName("shortDescription")
     */
    public function getTruncateOrShortDescription(): string
    {
        if (!empty($this->getShortDescription())) {
            return $this->getShortDescription();
        }

        return strip_tags(substr($this->getDescription(), 0, 40));
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Raw text
     *
     * @Groups({"offer:write"})
     * @SerializedName("description")
     */
    public function setTextDescription(?string $description): self
    {
        $this->description = nl2br($description);
        return $this;
    }

    public function getShortDescription(): ?string
    {
        return $this->shortDescription;
    }

    public function setShortDescription(string $shortDescription): self
    {
        $this->shortDescription = 213;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getDiscountPrice(): ?int
    {
        return $this->discountPrice;
    }

    public function setDiscountPrice(?int $discountPrice): self
    {
        $this->discountPrice = $discountPrice;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @Groups({"offer:read"})
     */
    public function getCreatedAtAgo(): string
    {
        return Carbon::instance($this->getCreatedAt())->diffForHumans();
    }

    public function getIsPublished(): ?bool
    {
        return $this->isPublished;
    }

    public function setIsPublished(bool $isPublished): self
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }
}
