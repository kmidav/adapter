<?php


namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Action\NotFoundAction;
use Symfony\Component\Serializer\Annotation\Groups;
use App\ApiPlatform\DailyReportDateFilter;
/**
 * Class DailyReport
 * @package App\Entity
 * @ApiResource(
 *     itemOperations={
 *         "get",
 *         "put"
 *     },
 *     collectionOperations={"get"},
 *     normalizationContext={"groups"={"daily-report:read"}},
 *     denormalizationContext={"groups"={"daily-report:write"}},
 *     paginationItemsPerPage=10
 * )
 * @ApiFilter(DailyReportDateFilter::class, arguments={"throwOnInvalid"=true})
 */
class DailyReport
{
    /**
     * @Groups({"daily-report:read"})
     */
    public $date;

    /**
     * @Groups({"daily-report:read", "daily-report:write"})
     */
    public $totalVisitors;

    /**
     * Топ 5 товаров дня
     *
     *
     * @Groups({"daily-report:read"})
     * @var array<ProductOffer>|ProductOffer[]
     */
    public $mostPopularProducts;

    /**
     *
     * @param \DateTimeInterface $date
     * @param int $totalVisitors
     * @param array $mostPopularProducts
     */
    public function __construct(\DateTimeInterface $date, int $totalVisitors, array $mostPopularProducts)
    {
        $this->date = $date;
        $this->totalVisitors = $totalVisitors;
        $this->mostPopularProducts = $mostPopularProducts;
    }

    /**
     * @ApiProperty(identifier=true)
     */
    public function getDateString(): string
    {
        return $this->date->format('Y-m-d');
    }
}