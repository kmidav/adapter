<?php

namespace App\Repository;

use App\Entity\AboutCompanyComment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AboutCompanyComment|null find($id, $lockMode = null, $lockVersion = null)
 * @method AboutCompanyComment|null findOneBy(array $criteria, array $orderBy = null)
 * @method AboutCompanyComment[]    findAll()
 * @method AboutCompanyComment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AboutCompanyCommentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AboutCompanyComment::class);
    }

    // /**
    //  * @return AboutCompanyComment[] Returns an array of AboutCompanyComment objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AboutCompanyComment
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
