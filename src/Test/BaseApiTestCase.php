<?php


namespace App\Test;


use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class BaseApiTestCase extends ApiTestCase
{
    protected function createUser(string $email, $username,  string $password): User
    {
        $user = new User();
        $user->setEmail($email);
        $user->setUsername($username);
        $encodedPassword = self::$container->get(UserPasswordEncoderInterface::class)
            ->encodePassword($user, $password);

        $user->setPassword($encodedPassword);

        $em = $this->getEntityManager();
        $em->persist($user);
        $em->flush();

        return $user;
    }


    protected function login(Client $client, string $email, string $password): void
    {
        $client->request('POST', '/login', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [
                'email' => $email,
                'password' => $password
            ],
        ]);
    }

    protected function createUserAndLogin(Client $client,string $email, string $username, string $password): User
    {
        $user = $this->createUser($email, $username, $password);
        $this->login($client, $email, $password);

        return $user;
    }

    protected function getEntityManager(): EntityManagerInterface
    {
        return self::$container->get(EntityManagerInterface::class);
    }
}