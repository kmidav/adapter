<?php


namespace App\Dto;


use App\Entity\User;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

// todo если используем Dto то все аннотации @Groups у ProductOffer убрать, они не нужны уже

class ProductOfferInput
{
    /**
     * @var string
     *
     * @Groups({"offer:write", "user:write"})
     * @Assert\NotBlank()
     * @Assert\Length(
     */
    public $name;

    /**
     * @var int
     * @Assert\NotBlank()
     * @Assert\GreaterThan(value="0")
     * @Groups({"offer:write", "user:write"})
     */
    public $price;

    /**
     *
     * @var User
     * @Groups({"offer:collection:post"})
     */
    public $owner;

    public $description;

    /**
     * @var bool
     */
    public $isPublished = false;

    /**
     * Raw text
     *
     * @Groups({"offer:write"})
     * @SerializedName("description")
     */
    public function setTextDescription(?string $description): self
    {
        $this->description = nl2br($description); # все \n заменит на <br>
        return $this;
    }
}