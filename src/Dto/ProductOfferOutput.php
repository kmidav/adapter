<?php


namespace App\Dto;


use App\Entity\User;
use Carbon\Carbon;
use Symfony\Component\Serializer\Annotation\Groups;

// todo если оставляем этот Dto класс то чистим ProductOffer т.е. убираем группы normalization context у свойств
// todo и ненужные методы удаляем, которые выовдят на лету генерируемые свойства
class ProductOfferOutput
{
    /**
     * Offer Name
     *
     * @Groups({"offer:read", "user:read"})
     * @var string
     */
    public $name;

    /**
     * @Groups({"offer:read"})
     * @var string
     */
    public $description;


    /**
     * @Groups({"offer:read"})
     * @var string
     */
    public $shortDescription;

    /**
     * @Groups({"offer:read", "user:read"})
     * @var int
     */
    public int $price;

    // просто для хранения данных
    public $createdAt;

    /**
     * @Groups({"offer:read"})
     * @var User
     */
    public $owner;

    /**
     * @return string
     * @Groups({"offer:read"})
     */
    public function getTruncateOrShortDescription(): string
    {
        if (!empty($this->shortDescription)) {
            return $this->shortDescription;
        }

        return strip_tags(substr($this->description, 0, 40));
    }

    /**
     * Date in "ago" format этот комментарий добавится в model (Schemas) внизу
     *
     * @Groups({"offer:read"})
     */
    public function getCreatedAtAgo(): string
    {
        return Carbon::instance($this->createdAt)->diffForHumans();
    }
}