<?php


namespace App\DataTransformer;


use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\ProductOfferOutput;
use App\Entity\ProductOffer;


class ProductOfferOutputDataTransformer implements DataTransformerInterface
{

    /**
     * @param ProductOffer $object
     * @param string $to
     */
    public function transform($object, string $to, array $context = [])
    {
        $output = new ProductOfferOutput();
        $output->name = $object->getName();
        $output->description = $object->getDescription();
        $output->shortDescription = $object->getShortDescription();
        $output->price = $object->getPrice();
        $output->createdAt = $object->getCreatedAt();
        $output->owner = $object->getOwner();

        return $output;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return $data instanceof ProductOffer && $to === ProductOfferOutput::class;
    }
}